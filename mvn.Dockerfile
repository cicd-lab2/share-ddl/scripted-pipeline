FROM maven:3.9.4-eclipse-temurin-17-alpine
WORKDIR /app
RUN mvn dependency:go-offline && \
    chown nobody:nobody /app
USER nobody
EXPOSE 8080
ENTRYPOINT [ "mvn", "spring-boot:run"]