# FROM maven:3.9.4-eclipse-temurin-17-alpine AS build
# WORKDIR /home/app
# COPY . .
# RUN mvn clean package -Dcheckstyle.skip && \
#     chown nobody:nobody /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar

# FROM maven:3.9.4-eclipse-temurin-17-alpinecat 
# WORKDIR /app
# COPY --from=build  /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar .
# RUN mkdir -p /app_logs && \
#     chown -R nobody:nobody /app_logs &&\
#     chmod -R 755 /app_logs
# USER nobody
# EXPOSE 8080
# CMD [ "sh", "-c","java -jar /app/spring-petclinic-3.1.0-SNAPSHOT.jar | tee -a /app_logs/logs.txt"]

# FROM maven:3.9.4-eclipse-temurin-17-alpine AS build
# WORKDIR /home/app
# COPY . .
# RUN mvn clean package -Dcheckstyle.skip && \
#     chown nobody:nobody /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar
# ENV USER_NAME=petclinic
# ENV UID=65534
# ENV gid=65534
# RUN addgroup --gid $gid $USER_NAME && \
#     adduser -u $UID -G $USER_NAME -D $USER_NAME

# FROM maven:3.9.4-eclipse-temurin-17-alpine
# WORKDIR /app
# COPY --from=build  /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar .
# RUN mkdir -p /app_logs && \ 
#     chown -R $USER_NAME:$USER_NAME /app_logs &&\
#     chmod -R 755 /app_logs
# USER $USER_NAME
# EXPOSE 8080
# CMD [ "sh", "-c","java -jar /app/spring-petclinic-3.1.0-SNAPSHOT.jar | tee -a /app_logs/logs.txt"]

FROM maven:3.9.4-eclipse-temurin-17-alpine AS build
WORKDIR /home/app
COPY . .
RUN mvn clean package -Dcheckstyle.skip  -Dskiptest && \
    chown nobody:nobody /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar

FROM maven:3.9.4-eclipse-temurin-17-alpine
WORKDIR /app
COPY --from=build /home/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar .
USER nobody
EXPOSE 8080
ENTRYPOINT [ "java" , "-jar", "/app/spring-petclinic-3.1.0-SNAPSHOT.jar"]

# CMD [ "sh", "-c","java -jar /app/spring-petclinic-3.1.0-SNAPSHOT.jar  2>&1 | tee -a /app_logs/logs.txt"]


# FROM maven:3.9.4-eclipse-temurin-17-alpine
# WORKDIR /app
# COPY . .
# RUN mvn clean package -Dcheckstyle.skip
# ENTRYPOINT [ "java" , "-jar", "/app/target/spring-petclinic-3.1.0-SNAPSHOT.jar"]


